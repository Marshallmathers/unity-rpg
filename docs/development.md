# *Developer Notes* #

## *Issue Solving Best Practices* ##

If you are assigned an issue here are some good things to follow.

## Create a branch with the name format something like ##

	git checkout -b <Name of Project>-<IssueNumber>
	
	The name of our project will be for now RPG
	So an appropriate branch title is RPG-2
	Therefore to create a branch for an issue shoudl be similar to
		git checkout -b RPG-2


## Commiting to this branch should be descriptive ##

	A good commit should have the name of the branch, a |, and some description

	Example: RPG-2 | "Added animations"

	This is to allow for organization, immediately an outside observer know that this commit is under the branch RPG-2 and that that person added new animation files.

	This is very useful when we merge branches into unstable and master where if something breaks we can track down the commit, branch, and change that caused the breakage.

	Example commit syntax: git commit -am "RPG-2 | Added animations"


## Pushing your branch ##

	Once you are happy with your current changes but are not yet done with your issue, then you can push your branch and also periodically update your branch.
	General practice is to make sure you pull master before pushing your branch to keep all your files up to date with the current working code.


	For example once you have committed your branch use
		git pull origin master
		git push origin <your branch>

	Some may see that you open up a weird text editor called vim when you pull master. This is an alert that you are merging master into your branch. To continue merging master into your branch just type :wq which stands for write(save) and quit. Merging master into your branch is usually conflict free but sometimes you may recieve a merge conflict. In which case flag Jack or Evan down and ask about it. Otherwise I recommend looking up ways to resolve this


## Pushing your branch to unstable ##

	Unstable's functionality is a testing ground for features that people belive work as intended. 

	To push to master:

		git pull origin master 		// Pull in latest code
		git push origin <yourbranch>

		Then you can make a pull request. Pull requests are ways of viewing changing and we can review the code to take out any errors we may see. Once it looks good we can approve it and it will merge into unstable.

		To create a pull request go to bitbucket and under actions on the left navigation bar and click create a pull request. Specify the branch you want to push into, in this case unstable. Give the pull request a title and a description then add Jack or Evan as the reviewer. Then create it and we should get the alert and review and push it into unstable. 
		Pull requests on github are similar if you cannot figure it out let Evan know.

		You may wonder what unstable is used for if we have master. Basically we will be testing approved code on unstable and if it works as expected Jack or Evan will pull unstable into master. It is the coder's responsibility to test their changes on unstable and make sure the game didn't break. Then let Evan or Jack know it works. We will double check and then either push into master from unstable, or give you some notes about not functioning code.

