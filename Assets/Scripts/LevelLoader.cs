﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {
	public string levelname;
	public string spawnPoint;
	
	private bool iamloading = false;

	GameObject player;

	void OnTriggerEnter(Collider other) {

		if (!iamloading) {
			if (other.tag.Equals ("Player")) {
				player = other.gameObject;
				foreach (GameObject obj in GameObject.FindGameObjectsWithTag ("Holdable")) { // bring whatever you're holding across with you
					if (obj.GetComponent<Holdable> ().held == true) {
						DontDestroyOnLoad (obj);
					} else {
						Destroy (obj);			// unless you're not actually holding it
					}
				}
				
				PhotonNetwork.LoadLevel (levelname);					//figure out how to add loading screen?
				
				DontDestroyOnLoad (gameObject);
				iamloading = true;
				//	PositionSaver ps = other.GetComponent<PositionSaver>();
				//	ps.loadLevel(levelname, Application.loadedLevelName, transform.position, transform.rotation);
				
				Debug.Log ("Level \"" + levelname + "\" Loaded");
			}
		}
	}
	
	void OnTriggerStay(Collider other) {
		if (other.tag.Equals ("Player")) {
			Debug.Log ("Player in Triggerzone");
		}
	}
	void OnTriggerExit(Collider other) {
		if (other.tag.Equals ("Player")) {
			Debug.Log ("Player exited Triggerzone");
		}
	}
	
	void OnLevelWasLoaded(){
		if (iamloading) {
			foreach (GameObject obj in GameObject.FindGameObjectsWithTag ("Spawnpoint")) { 
				if (obj.GetComponent<CharacterSpawn> ().name.Equals(spawnPoint)){ 
					Debug.Log("Spawning at " + spawnPoint);
					obj.GetComponent<CharacterSpawn> ().SpawnPlayer(player);
				} else {
					Debug.Log ("not at " + obj.GetComponent<CharacterSpawn> ().name);
					Destroy(obj.GetComponent<CharacterSpawn> ());
				}
			}
			Destroy (this.gameObject);
		}
		
	}
}