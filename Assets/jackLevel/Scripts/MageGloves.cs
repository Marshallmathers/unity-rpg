﻿using UnityEngine;
using System.Collections;

public class MageGloves : Holdable {
	//serialize element for bonus / reduced damage against elemental enemies
	private MagicElement[] elements = {
		new MagicElement ("fire", new Color(1, 0.3f, 0), -1.0f), 	
		new MagicElement ("water", Color.blue, 0.5f), 
		new MagicElement ("wind", Color.white, 0.0f), 
		new MagicElement ("magi", Color.magenta, 0.0f),
		new MagicElement ("lightning", Color.yellow, 0.0f)};
	private MagicElement right;
	public ParticleSystem rightParticle;
	public ParticleSystem rightShoot;
	private MagicElement left;
	public ParticleSystem leftParticle;
	public ParticleSystem leftShoot;
	
	// Use this for initialization
	void Start () {
		rightShoot.enableEmission = false;
		leftShoot.enableEmission = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (held) {
			if (!Input.GetButton("Activate")) {
				rightParticle.enableEmission = true;
				rightShoot.enableEmission = false;
				//do raycasting hits
			}
			if (!Input.GetButton ("AltActivate")) {
				leftParticle.enableEmission = true;
				leftShoot.enableEmission = false;
				//do raycasting hits
			}
			if (Input.GetKeyDown (KeyCode.E)) {
				right = elements [Random.Range (0, elements.Length)];
				rightParticle.startColor = right.color;
				rightParticle.gravityModifier = right.gravityMultiplier;
				rightShoot.startColor = right.color;
			}
			
			if (Input.GetKeyDown (KeyCode.Q)) {
				left = elements [Random.Range (0, elements.Length)];
				leftParticle.startColor = left.color;
				leftParticle.gravityModifier = left.gravityMultiplier;
				leftShoot.startColor = left.color;
			}

		//	if (Input.GetMouseButton (1)) {
		//		rightParticle.enableEmission = false;
		//		rightShoot.enableEmission = true;
				//do raycasting hits
		//	}
		//	if (Input.GetMouseButton (0)) {
		//		leftParticle.enableEmission = false;
		//		leftShoot.enableEmission = true;
		//		//do raycasting hits
		//	}
		}
	}
	
	public override void Activate(){
		Debug.Log("Bloop");
		if (Input.GetButton("Activate")) {
			rightParticle.enableEmission = false;
			rightShoot.enableEmission = true;
			//do raycasting hits
		}
		if (Input.GetButton ("AltActivate")) {
			leftParticle.enableEmission = false;
			leftShoot.enableEmission = true;
			//do raycasting hits
		}
	}
}

class Element{
	public Color color;
	public float gravityMultiplier;
	public string name;
	//particletexture
	
	public Element(string name, Color color, float gravityMultiplier){
		this.color = color;
		this.name = name;
		this.gravityMultiplier = gravityMultiplier;
	}
}