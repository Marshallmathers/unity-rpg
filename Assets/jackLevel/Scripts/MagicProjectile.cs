﻿using UnityEngine;
using System.Collections;

public class MagicProjectile : MonoBehaviour {
	//serialize element for bonus / reduced damage against elemental enemies
//	private MagicElement[] elements = {
//		new MagicElement ("fire", Color.red, -1.0f), 	
//		new MagicElement ("water", Color.blue, 0.5f), 
//		new MagicElement ("wind", Color.white, 0.0f), 
//		new MagicElement ("magi", Color.magenta, 0.0f),
//		new MagicElement ("lightning", Color.yellow, 0.0f)};
	public MagicElement element;
	private float damage;
	
	
	public ParticleSystem particles;
	
	
	// Use this for initialization
	void Start () {
	//	for (int i = 0; i < elements.Length; i ++) {
//			if (elements[i].name.Equals(elementstr))
//				element = elements[i];
//		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void onTriggerEnter(Collider other){
		if (other.GetComponent<DamageEntity> () != null) {
			other.GetComponent<DamageEntity> ().elementalHit(damage, element.name);
		}
		
	//	Destroy (this.gameObject);
	}

	public void setElement ( MagicElement lmnt ) {
		element = lmnt;
		particles.startColor = element.color;
	}
}

public class MagicElement{
	public Color color;
	public float gravityMultiplier;
	public string name;
	//particletexture
	
	public MagicElement(string name, Color color, float gravityMultiplier){
		this.color = color;
		this.name = name;
		this.gravityMultiplier = gravityMultiplier;
	}
}